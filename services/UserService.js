planesonly.factory('UserService', ['$firebaseAuth', '$location', '$localStorage', function ($firebaseAuth, $location, $localStorage) {
  return {
     // get user details
    getUserIdAsync: function (successCallback) {
      $firebaseAuth().$onAuthStateChanged(function (user) {
        var userIdobj = user
        successCallback(userIdobj)
      }
      )
    },
    getUserIdSync: function (successCallback) {
      successCallback($firebaseAuth().$getAuth())
    },
    // get username from firebase database
    getUserName: function () {
      return firebase.database().ref('/users/' + $firebaseAuth().$getAuth().uid)
        .once('value')
        .then(function (snapshot) {
          var userName = (snapshot.val() && snapshot.val().name) || 'Anonymous'
          return userName
        })
    }
  }
}])
