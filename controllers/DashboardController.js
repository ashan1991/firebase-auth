planesonly.controller('DashboardController', ['$scope', '$firebaseAuth', '$location', '$localStorage', '$window', 'UserService', function ($scope, $firebaseAuth, $location, $localStorage, $window, UserService) {
  $scope.uName = ''
  // check userId exists or not and redirect to login
  UserService.getUserIdSync(function (user) {
    $scope.userIdObj = user
    if ($scope.userIdObj == null) {
      $location.path('/')
      $scope.$apply()
    } else {
      // get username from firebase database and show on dashboard
      UserService.getUserName().then(function (data) {
        $scope.uName = data
        $scope.$apply()
        $scope.authAsyn()
      }).catch(function () { $scope.authAsyn() })
    }
  })

  $scope.authAsyn = function () {
    UserService.getUserIdAsync(function (user) {
      $scope.userIdObj = user
      if ($scope.userIdObj == null) {
        $location.path('/')
        $scope.$apply()
      }
    })
  }

  $scope.signOut = function () {
    // sign out from dashboard, clear local storage and redirect to login page
    $firebaseAuth().$signOut()
    $window.localStorage.clear()
    $location.path('/')
  }
}])
