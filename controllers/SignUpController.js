planesonly.controller('SignUpController', ['$scope', 'FireBaseAuth', '$location', '$localStorage', 'UserService', function ($scope, FireBaseAuth, $location, $localStorage, UserService) {
  // check userId exists and redirect to dashboard
  UserService.getUserId(function (user) {
    $scope.UserIdObj = user
    if ($scope.UserIdObj != null) {
      $location.path('/dashboard')
      $scope.$apply()
    }
  })

  $scope.createUser = function () {
    $scope.error = null
    // creating user's account on firebase authentication, database and redirect to dashboard
    FireBaseAuth.$createUserWithEmailAndPassword($scope.user.email, $scope.user.password, $scope.user.name)
            .then(function (firebaseUser) {
              var userId = firebaseUser.uid
              var ref = firebase.database().ref().child('users').child(userId)
              ref.update({
                name: $scope.user.name,
                email: firebaseUser.email
              })
              if (userId != null) {
                $location.path('/dashboard')
                // create a object included user id on localstorage
                $localStorage.userObj = {
                  planesonlyUid: userId
                }
              }
            }).catch(function (error) {
              // User validation
              var signUpError = error.code
              if (signUpError === 'auth/email-already-in-use') {
                $scope.errorSignUp = 'Email is aready registered'
              } else if (signUpError === 'auth/weak-password') {
                $scope.errorSignUp = 'Weak Password'
              }
            })
  }
}])
