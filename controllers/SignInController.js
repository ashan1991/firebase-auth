planesonly.controller('SignInController', ['$scope', '$firebaseAuth', '$location', '$localStorage', '$window', 'UserService', function ($scope, $firebaseAuth, $location, $localStorage, $window, UserService) {
  // check userId exists and redirect to dashboard
  UserService.getUserIdSync(function (user) {
    $scope.UserIdObj = user
    if ($scope.UserIdObj != null) {
      $location.path('/dashboard')
      $scope.$apply()
    }
  })
  $scope.login = function () {
    // sign in using email and password.
    $firebaseAuth().$signInWithEmailAndPassword($scope.user.email, $scope.user.password).then(function () {
      $location.path('/dashboard')
      // createing user id object on localStorage
    }).catch(function (error) {
      // User validation
      var signInError = error.code
      if (signInError === 'auth/user-not-found' || signInError === 'auth/wrong-password') {
        $scope.errorSignIn = 'Wrong Email or Password'
      }
    })
  }
}])
