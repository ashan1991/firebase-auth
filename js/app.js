var config = {
  apiKey: 'AIzaSyBlZvDspUs0VkfPw4vOFALvETrIxBRaAWA',
  authDomain: 'planesonly-ff7d7.firebaseapp.com',
  databaseURL: 'https://planesonly-ff7d7.firebaseio.com',
  projectId: 'planesonly-ff7d7',
  storageBucket: 'planesonly-ff7d7.appspot.com',
  messagingSenderId: '11492712310'
}

firebase.initializeApp(config)

var planesonly = angular.module('planesonly', ['ngRoute', 'firebase', 'ngStorage'])

planesonly.config(function ($routeProvider) {
  $routeProvider
  .when('/register', {
    templateUrl: 'register.html',
    controller: 'SignUpController'
  })
  .when('/dashboard', {
    templateUrl: 'dashboard.html',
    controller: 'DashboardController'
  })
  .otherwise({
    templateUrl: 'login.html',
    controller: 'SignInController'
  })
})
